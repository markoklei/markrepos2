#include <iostream>
#include <condition_variable>
#include <thread>
#include <mutex>
#include <chrono>
#include <vector>

using namespace std;

mutex mtx;
int empty_counter = 0; // When this variable is zero it means that the file is empty.

void read(int num_of_thread) // Unfortunately , this function cannot support multiple readers. :(
{
	while (empty_counter == 0){
		this_thread::sleep_for(chrono::seconds(1));
	}

	unique_lock<mutex> mt(mtx);

	cout << "Thread number " << num_of_thread << " is reading." << endl; // Simply printing for visual results.

	empty_counter -= 1; // Every time we read we "take" some of the file's information hence, we decrease the variable.
	mt.unlock();
}

void write(int num_of_thread)
{
	unique_lock<mutex> mt(mtx);

	cout << "Thread number " << num_of_thread << " is writing." << endl; // Simply printing for visual results.

	empty_counter += 1; // Every time we write we "add" some information to the file hence, we increase the variable.
	mt.unlock();
}

int main() { 
	thread arr_thrd[4];
	// Assigning the array with activated threads.
	arr_thrd[0] = thread(read,1);
	arr_thrd[1] = thread(read,2);
	arr_thrd[2] = thread(write,1);
	arr_thrd[3] = thread(write,2);
	// Here ends the array assignment I mentioned above.
	
	for (int i = 0; i < 4; i++){
		arr_thrd[i].join(); /* At first, the threads are working on their own, without any connection with each other.
								When we commence the join command we let them work *with* each other, synchronized. */
	}

system("PAUSE");
return 0;
}