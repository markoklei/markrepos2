#include <windows.h> <WINDOWS.H>
#include <strsafe.h> <STRSAFE.H>
#include <stdio.h><STDIO.H>

#define BUF_SIZE 255
#define HUGE 999
//Display message function
void DisplayMessage(HANDLE hScreen,
	char *ThreadName, int Data, int Count)
{

	TCHAR msgBuf[BUF_SIZE];
	size_t cchStringSize;
	DWORD dwChars;

	StringCchLength(msgBuf, BUF_SIZE, &cchStringSize);
	WriteConsole(hScreen, msgBuf, cchStringSize,
		&dwChars, NULL);
	Sleep(1000);
}

DWORD WINAPI Thread_no_1(LPVOID lpParam)
{

	int     Data = 0;
	int     count = 0;
	HANDLE  hStdout = NULL;

	if ((hStdout =GetStdHandle(STD_OUTPUT_HANDLE))== INVALID_HANDLE_VALUE)
		return 1;

	Data = *((int*)lpParam);
	for (count = 0; count <= HUGE; count++)
	{
		DisplayMessage(hStdout, "Thread_no_1", Data, count);
	}
	return 0;
}

void main()
{
	int Data_Of_Thread_1 = 1;
	int Data_Of_Thread_2 = 2;
	int Data_Of_Thread_3 = 3;
	HANDLE Handle_Of_Thread_1 = 0; 
	HANDLE Handle_Of_Thread_2 = 0;
	HANDLE Handle_Of_Thread_3 = 0;
	HANDLE Array_Of_Thread_Handles[HUGE];

	Handle_Of_Thread_1 = CreateThread(NULL, 0,Thread_no_1, &Data_Of_Thread_1, 0, NULL);
	if (Handle_Of_Thread_1 == NULL)
		ExitProcess(Data_Of_Thread_1);

	Array_Of_Thread_Handles[0] = Handle_Of_Thread_1;

	// Wait until threads are killed.
	WaitForMultipleObjects(1 , Array_Of_Thread_Handles, TRUE, INFINITE);

	printf("Since All threads executed lets close their handles \n");

	// Close all thread handles.
	CloseHandle(Handle_Of_Thread_1);
}